<!DOCTYPE HTML>
<!DOCTYPE html>
<html>
<head>
	<title>SPT-Test</title>

	<link rel="stylesheet" href="css/display.css" type="text/css" />
</head>
<body>

 <?php include 'header.php';?>

<div class="fmargin"></div>
<h4>Your Input data</h4>
<?php
	$E_m = $_POST['HammerType'];
	$C_b = $_POST['BoreholeDiameter'];
	$C_s = $_POST['SoilType'];
	$C_r = $_POST['RodLength'];

	$C_p = $_POST['Pressure'];

	$nValue = $_POST['blows'];

	if($E_m == HT1){
		$Hammer_eff = 0.55;
	}

	if($C_b == BD1){
		$boreholeDiameters = 1.00;
	}
	if($C_b == BD2){
		$boreholeDiameters = 1.05;
	}
	if($C_b == BD3){
		$boreholeDiameters = 1.15;
	}

	if($C_s == ST1) {
		$soilValue = 1.00;
	}
	if($C_s == ST2) {
		$soilValue = 0.80;
	}
	if($C_s == ST3) {
		$soilValue = 0.90;
	}

	if($C_r == RL1){
		$roadLengthValue = 0.75;
	}
	if($C_r == RL2){
		$roadLengthValue = 0.85;
	}
	if($C_r == RL3){
		$roadLengthValue = 0.95;
	}
	if($C_r == RL4){
		$roadLengthValue = 1.00;
	}


	$N_60 = (($nValue)*($Hammer_eff)*($boreholeDiameters)*($soilValue)*($roadLengthValue))/0.60;

	
	if($C_p >= 75){
		$C_n = (4)/(1+4*$C_p);
	}
	else{
		$C_n = (4)/(3.25+1.01*$C_p);
	}
	
	echo "
		<table width='100%', border='1px solid black'>
			<tr>
				<th> Equipment variables</th>
				<th> Factor </th>
				<th> Values </th>
			</tr>
			<tr>
				<td>Donut hammer</td>
				<td>Hammer efficiency, Em </td>
				<td>$Hammer_eff</td>
			</tr>
			<tr>
				<td>Rod length </td>
				<td>Rod length factor, Cr</td>
				<td>$roadLengthValue </td>
			</tr>
			<tr>
				<td>Borehole </td>
				<td>Borehole diameter factor, Cb</td>
				<td>$boreholeDiameters </td>
			</tr>
			<tr>
				<td>Sampling Method </td>
				<td>Sampling Method factor, Cs</td>
				<td>$soilValue </td>
			</tr>
			<tr>
				<td>Pressure </td>
				<td>Pressure, (sigmao)</td>
				<td>$C_p</td>
			</tr>
		</table>
	";


	$Correction = $C_n*$N_60;


	if($Correction > 15){
		$N = 0.6*$Correction;
	}
	else{
		$N = $Correction;
	}


	if($N_60 >=0 && $N_60 <=5){
		$S_d1 = "Very loose";
		$Dr = "0-5";
	}
	if($N_60 >=5 && $N_60 <10){
		$S_d1 = "loose";
		$Dr = "5-30";
	}
	if($N_60 >=10 && $N_60 <30){
		$S_d1 = "Medium dense";
		$Dr = "30-60";
	}
	if($N_60 >=30 && $N_60 <60){
		$S_d1 = "Dense";
		$Dr = "60-95";
	}
?>
<div class="fmargin"></div>
<h4>Your Output data based on N60</h4>


<?php

	echo "
		<table width='100%', border='1px solid black'>
			<tr>
				<th> N60</th>
				<th> Soil Type</th>
				<th> Dr(%) </th>
			</tr>
			<tr>
				<td>$N_60</td>
				<td>$S_d1</td>
				<td>$Dr</td>
			</tr>
		</table>
	";
/*
value  = cn
value = n'
value = n''
*/

if($nValue >=0 && $nValue <=4){
		$S_d1 = "Very loose";
		$Dr = "0-15";
		$p1 = "<28";
	}
	if($nValue >=5 && $nValue <10){
		$S_d1 = "loose";
		$Dr = "15-35";
		$p1 = "28-30";
	}
	if($nValue >=10 && $nValue <30){
		$S_d1 = "Medium dense";
		$Dr = "35-65";
		$p1 = "30-36";
	}
	if($nValue >=30 && $nValue <50){
		$S_d1 = "Dense";
		$Dr = "65-85";
		$p1 = "36-41";
	}
	if($nValue >= 50){
		$S_d1 = "Very dense";
		$Dr = ">85";
		$p1 = ">41";
	}
?>
	<div class="fmargin"></div>
<h4>Your Output data based on N</h4>
<?php

echo "
		<table width='100%', border='1px solid black'>
			<tr>
				<th> N</th>
				<th> Soil Type</th>
				<th> Dr(%) </th>
				<th>Phy</th>
			</tr>
			<tr>
				<td>$nValue</td>
				<td>$S_d1</td>
				<td>$Dr</td>
				<td>$p1</td>
			</tr>
		</table>
	";


?>

<div class="fmargin"></div>
<?php include 'footer.php';?>
</body>
</html>