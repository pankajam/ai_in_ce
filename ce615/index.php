<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>SPT-Test</title>
	<meta charset="UTF-8">

	<link rel="stylesheet" href="css/main.css" type="text/css" />
</head>
<body>
	 <?php include 'header.php';?>
	 	<div class="MarginTop"></div>
	 	<div class="setup">
	 		<img src="images/SPT-setup.jpg" alt="spt-setup" /></a>
	 	</div>

	 
<div class="info">
	 		<h4>Some important information(References) about SPT Test</h4> 
	 		<ul>
	 			<li><a href="https://www.youtube.com/watch?v=Qfc39krdFa8">SPT Test at IIT Guwahati</a></li>
				<li><a href="https://www.academia.edu/7889537/Use_of_SPT_Blow_Counts_to_Estimate_Shear_Strength_Properties_of_Soils_Energy_Balance_Approach">Technical notes on academia</a></li>
 	 		</ul>
	 	</div>

	 <div class="MarginTop"></div>

	<?php include 'footer.php';?>

</body>
</html>