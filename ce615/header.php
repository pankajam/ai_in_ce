<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>SPT-Test</title>
	<meta charset="UTF-8">

	<link rel="stylesheet" href="css/header.css" type="text/css" />
</head>
<body>
	<div class="header-wrap">
  <div class="header">
    <div class="menu">
      <ul>
        <li><a href="index.php" class="active">Home</a></li>
        <li><a href="services.php">Services</a></li>
        <li><a href="#">About Us</a></li>
        <li><a href="#">Contact Us</a></li>
      </ul>
    </div>
    <div class="social-media">
      <ul>
        <li><a href="#"><img src="images/facebook.gif" alt="facebook" /></a></li>
        <li><a href="#"><img src="images/linkedin.gif" alt="linkedin" /></a></li>
        <li><a href="#"><img src="images/twitter.gif" alt="twitter" /></a></li>
      </ul>
    </div>
  </div>
</div>

<div class="logo-sarch-wrap">
  <div class="logo-search-container">
    <div class="logo">
      <h1>SPT-Test</h1>
    </div>
    <!-- <div class="search">
      <div class="search-input">
        <input type="text" class="search-text-field" value="Search Here ..." />
      </div>
      <div class="search-button"><a href="#"><img src="images/serach-icon.gif" alt="search" /></a></div>
    </div> -->
  </div>
</div>

</body>
</html>