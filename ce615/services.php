<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>SPT-Test</title>
	<meta charset="UTF-8">

	<link rel="stylesheet" href="css/services.css" type="text/css" />
</head>
<body>

<?php include 'header.php';?>

<div class="serv-assumption">
<h4>Some Standard assumption for SPT Test</h4>
	<div class="assumption_table">
		<table style="width:100%">
  			<!-- <tr>
	   			 <th>Length of standard split spoon sampler</th>
	   			 <th>Lastname</th> 
  			</tr> -->
  			 <tr>
			    <td>Length of standard split spoon sampler</td>
			    <td>450 mm</td> 
  			</tr>
  			<tr>
			    <td>Height of fall</td>
			    <td>750 mm</td> 
  			</tr>
  			<tr>
			    <td>Standard hammer weight</td>
			    <td>63.5 kg</td> 
  			</tr>
  			<tr>
			    <td>SPT N value</td>
			    <td>number of blows for 300 mm</td> 
  			</tr>
	</table>
	</div>
</div>

<div class="MarginTop"></div>

<div class="user-data">
	<form action="display.php" method="POST">

	<table>
		<tr>
			<td>Entre Hammer Type</td>
			<td>
				<select name="HammerType">
    				<option value="HT1">Donut hammer</option>
  				</select>
			</td>
		</tr>
		<tr>
			<td>Entre Borehole diameter</td>
			<td>
				<select name="BoreholeDiameter">
    				<option value="BD1">60-120 mm</option>
    				<option value="BD2">150 mm</option>
   					<option value="BD3">200 mm</option>
  				</select>
			</td>
		</tr>
		<tr>
			<td>Entre Rod length range</td>
			<td>
				<select name="RodLength">
				    <option value="RL1">3-4 m</option>
				    <option value="RL2">4-6 m</option>
				    <option value="RL3">6-10 m</option>
				    <option value="RL4"> >10 m</option>
 				 </select>
			</td>
		</tr>
		<tr>
			<td>Entre Soil Type</td>
			<td>
				<select name="SoilType">
				    <option value="ST1">Standard sampler With liner</option>
				    <option value="ST2">Dense sand</option>
				    <option value="ST3">clay Loose sand</option>
  				</select>
			</td>
		</tr>
		<tr>
			<td>Entre Overburden pressure(kPa)</td>
			<td>
			  <input type="text" name="Pressure"><br>
			</td>
		</tr>
		<tr>
			<td>Entre number of blows</td>
			<td>
			  <input type="text" name="blows"><br>
			</td>
		</tr>
		<tr>
			<td><input type="submit"></td>
		</tr>	
	</table>
	</form>
	</div>

<div class="MarginTop"></div>
<?php include 'footer.php';?>
</body>
</html>
